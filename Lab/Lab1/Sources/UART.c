
UART_Init(const uint32_t baudRate, const uint32_t moduleClk);

UART_InChar(uint8_t * const dataPtr);

UART_OutChar(const uint8_t data);


void UART_Poll(void);
